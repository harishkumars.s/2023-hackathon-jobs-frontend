import { Link } from 'react-router-dom';
import Main from './Main';
import { List } from 'antd';
import './css/App.css';

function App() {

  const links = [
    '/newJob',
    '/jobs'
  ];

  const linkMap = new Map();
  linkMap.set('/newJob', 'New job posting')
  linkMap.set('/jobs', 'Job postings')

  return (
    <div className='appContainer'>
      <table className='appTable'>
        <tbody>
          <tr className='appRow'>
            <td className='appLeftPane'>
              <>
                <List
                  size="small"
                  header={<div><b>Jobs</b></div>}
                  dataSource={links}
                  renderItem={(item) => <List.Item>
                    <Link to={item}>{linkMap.get(item)}</Link>
                  </List.Item>}
                />
              </>
            </td>
            <td className="appRightPane">
              <Main />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default App;
