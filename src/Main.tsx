import { Routes, Route } from 'react-router-dom';
import NewJob from './components/NewJob';
import Jobs from './components/Jobs';
import ApplyJob from './components/ApplyJob';
import ViewApplicants from './components/ViewApplicants';
import './css/Form.css'

const Main = () => {
  return (
    <div>
      <Routes>
        <Route path='/newJob' element={<NewJob />} />
        <Route path='/jobs' element={<Jobs />} />
        <Route path='/applyJob' element={<ApplyJob />} />
        <Route path='/getApplicants' element={<ViewApplicants />} />
      </Routes>
    </div>
  );
}
export default Main;
