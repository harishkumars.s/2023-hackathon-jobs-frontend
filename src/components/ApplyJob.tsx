import { useForm, SubmitHandler } from "react-hook-form";
import { useState, useEffect } from 'react';
import { Typography, Alert, } from 'antd';
import { useLocation } from 'react-router-dom';
import { applyForJob } from "../services/jobs";

const { Title } = Typography;

type Inputs = {
  jobId: string;
  name: string;
  email: string;
  mobile: string;
};

export default function ApplyJob() {

  const title = 'Apply for Job'

  const state = useLocation().state;

  useEffect(() => {
    document.title = title
  }, []);

  const [errorMsg, setErrorMsg] = useState<string>('');
  const [successMsg, setSuccessMsg] = useState<string>('');

  const { register, handleSubmit, reset, formState: { errors } } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (formData) => {
    setErrorMsg('')
    setSuccessMsg('')
    reset()

    formData.jobId = state;
    const success = await applyForJob(formData)
    if (success) {
      setSuccessMsg('Submitted new job application')
    } else {
      setErrorMsg('Failed to post application.')
    }
  }

  return (
    <div>
      <Title>{title}</Title>
      {errorMsg && <Alert message={errorMsg} type="error" />}
      {errorMsg && <br></br>}
      {successMsg && <Alert message={successMsg} type="success" />}
      {successMsg && <br></br>}
      <form onSubmit={handleSubmit(onSubmit)}>
        <input className='formInput' placeholder='Full name' {...register('name', { required: true })} />
        {errors.name && <span className='formSpan'>Name is required</span>}
        <input className='formInput' placeholder='Email address' {...register('email', { required: true })} />
        {errors.email && <span className='formSpan'>Email is required</span>}
        <input className='formInput' placeholder='Mobile number' {...register('mobile', { required: true })} />
        {errors.mobile && <span className='formSpan'>Mobile is required</span>}
        <input value='Submit' className='formSubmit' type="submit" />
      </form>
    </div>
  );
}