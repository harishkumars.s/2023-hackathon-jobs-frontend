import { useState, useEffect } from 'react';
import { Table, Space, Typography, Button, Spin, Alert } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Link } from 'react-router-dom';
import { getJobs, deleteJob } from '../services/jobs'

const { Title } = Typography;

const title = 'Jobs'
const loadingMessage = 'Fetching job postings from server. Please wait.'
const rowKey = '_id'

// list of columns in the table
interface IColumns {
  title: string;
  desc: string;
  location: string;
}

export default function Jobs(props: any) {

  function showElements(isDataLoading: boolean, columns: ColumnsType<IColumns>, data: any[]) {
    if (isDataLoading) {
      return <Spin tip='' size='large'>
        <Alert
          message={loadingMessage}
          description=''
          type='info'
        />
      </Spin>
    }
    return <Table<IColumns> columns={columns} dataSource={data} rowKey={rowKey} />
  }

  // initial data loaded from backend
  const [initialState, setInitialState] = useState<any>(null)

  // used to force UI refresh
  const [isDataUpdated, setIsDataUpdated] = useState<boolean>(false)

  // used to show data loading state
  const [isDataLoading, setIsDataLoading] = useState<boolean>(false)

  useEffect(() => {
    document.title = title;

    // fetch initial data from backend
    (async () => {
      setIsDataLoading(true)
      const response = await getJobs()
      setInitialState(response)
      setIsDataLoading(false)
    })();
  }, [isDataUpdated]);

  const tableColumns: ColumnsType<IColumns> = [
    {
      title: 'Title',
      dataIndex: 'title',
    },
    {
      title: 'Location',
      dataIndex: 'location',
    },
    {
      title: 'Action',
      dataIndex: '_id',
      render: (jobId) => {
        return <Space size='large'>
          <Button danger onClick={async (e) => {
            setIsDataUpdated(false)
            await deleteJob(jobId)
            // update state so that UI gets refreshed
            setIsDataUpdated(true)
          }}>Delete</Button>
          <Link to='/getApplicants' state={jobId}>View applicants </Link>
          <Link to='/applyJob' state={jobId}>Apply</Link>
        </Space>
      }
    },
  ];

  return (
    <div>
      <Title>{title}</Title>
      {showElements(isDataLoading, tableColumns, initialState)}
    </div>
  );
}