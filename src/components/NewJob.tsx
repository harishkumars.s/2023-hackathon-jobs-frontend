import { useForm, SubmitHandler } from "react-hook-form";
import { useState, useEffect } from 'react';
import { Typography, Alert } from 'antd';
import { createJob } from "../services/jobs";

const { Title } = Typography;

type Inputs = {
  title: string;
  desc: string;
  location: string;
};

export default function NewJob() {

  const title = 'New Job Posting'

  useEffect(() => {
    document.title = title
  }, []);

  const [errorMsg, setErrorMsg] = useState<string>('');
  const [successMsg, setSuccessMsg] = useState<string>('');

  const { register, handleSubmit, reset, formState: { errors } } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async (formData) => {
    setErrorMsg('')
    setSuccessMsg('')
    reset()

    const success = await createJob(formData)
    if (success) {
      setSuccessMsg('Posted a new job')
    } else {
      setErrorMsg('Failed to post job.')
    }
  }

  return (
    <div>
      <Title>{title}</Title>
      {errorMsg && <Alert message={errorMsg} type="error" />}
      {errorMsg && <br></br>}
      {successMsg && <Alert message={successMsg} type="success" />}
      {successMsg && <br></br>}
      <form onSubmit={handleSubmit(onSubmit)}>
        <input className='formInput' placeholder='Title' {...register('title', { required: true })} />
        {errors.title && <span className='formSpan'>Title is required</span>}
        <textarea className='formTextarea' placeholder='Description' {...register('desc', { required: true })} />
        {errors.desc && <span className='formSpan'>Description is required</span>}
        <input className='formInput' placeholder='Location' {...register('location', { required: true })} />
        {errors.location && <span className='formSpan'>Location is required</span>}
        <input value='Create' className='formSubmit' type="submit" />
      </form>
    </div>
  );
}