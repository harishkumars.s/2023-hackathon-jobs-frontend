import { useState, useEffect } from 'react';
import { Table, Space, Typography, Spin, Alert } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Link, useLocation } from 'react-router-dom';
import { getJobApplications } from '../services/jobs'

const { Title } = Typography;
const title = 'Job applications'
const loadingMessage = 'Fetching job applications from server. Please wait.'
const rowKey = '_id'

// list of columns in the table
interface IColumns {
  id: string;
  name: string;
  email: string;
  mobile: string;
}

const tableColumns: ColumnsType<IColumns> = [
  {
    title: 'Full name',
    dataIndex: 'name',
  },
  {
    title: 'Email address',
    dataIndex: 'email',
  },
  {
    title: 'Mobile number',
    dataIndex: 'mobile',
  },
  {
    key: '_id',
    title: 'Action',
    dataIndex: '_id',
    render: (data) => {
      // TODO: Accept or Reject actions are not implemented yet.
      return <Space size='large'>
        <Link to='' state={data}>Accept</Link>
        <Link to='' state={data}>Reject</Link>
      </Space>
    }
  },
];

export default function Jobs(props: any) {

  function showElements(isDataLoading: boolean, columns: ColumnsType<IColumns>, data: any[]) {
    if (isDataLoading) {
      return <Spin tip='' size='large'>
        <Alert
          message={loadingMessage}
          description=''
          type='info'
        />
      </Spin>
    }
    return <Table<IColumns> columns={columns} dataSource={data} rowKey={rowKey} />
  }

  // initial data loaded from backend
  const [initialState, setInitialState] = useState<any>(null)

  // used to force UI refresh
  const [dataUpdated, setIsDataUpdated] = useState<boolean>(false)

  // used to show data loading state
  const [isDataLoading, setIsDataLoading] = useState<boolean>(false)

  // data required to initiate fetch
  const jobId = useLocation().state;

  useEffect(() => {
    document.title = title;

    // fetch initial data from backend
    (async () => {
      setIsDataLoading(true)
      const response = await getJobApplications(jobId)
      setInitialState(response)
      setIsDataLoading(false)
    })();
  }, [dataUpdated]);

  return (
    <div>
      <Title>{title}</Title>
      {showElements(isDataLoading, tableColumns, initialState)}
    </div>
  );
}
