
// get list of jobs
// TODO: gets list of all jobs. Need to filter based on user id
export async function getJobs() {
    const response = await fetch('/jobs', {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    })
    if (response.ok) {
        return await response.json()
    }
    else {
        return ''
    }
}

// get applications for a given job
export async function getJobApplications(jobId: string) {
    const requestBody = JSON.stringify({ 'jobId': jobId })
    const response = await fetch('/getApplicants', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: requestBody
    })
    if (response.ok) {
        return await response.json()
    }
    else {
        return ''
    }
}

// delete job (and its applicants)
export async function deleteJob(jobId: string) {
    const requestBody = JSON.stringify({ 'jobId': jobId })
    await fetch('/deleteJob', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: requestBody
    })
}

// create a new job posting
export async function createJob(job: any) {
    const response = await fetch('/createJob', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(job)
    })
    return response.ok
}

// create a new job application
export async function applyForJob(application: any) {
    const response = await fetch('/applyJob', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(application)
    })
    return response.ok
}
